Requires [BYOND](https://secure.byond.com) in order to play.

Requires [Kaiochao's Absolute Positions](http://www.byond.com/developer/Kaiochao/absolutepositions) and [Kaiochao's Directions](http://www.byond.com/developer/Kaiochao/Directions) libraries to compile.

Capable of running on machines as low-spec as a Pentium 4.