mob/proc/newCharacter()
	set waitfor = 0
	race = input("What race do you wish to be?") in allowedRaces
	var/race/R = global.races[race]
	if(R.gendered)
		var option = input("What gender do you wish to be?") in list("Male", "Female")
		gender = lowertext(option)
	startStats(R.statMin, R.statMax, R.statPoints, _cancel = 1)
	while(statChoosing)
		sleep(2)
	if(statFinished)
		redoStat(statSetup)
		newCharacterSetup()
	else
		characterStart()
		race = initial(race)
mob/proc/characterStart() // Called at login, to load or make a new character.
	var option = input("New or Load?") in list("New", "Load")
	switch(option)
		if("New")
			newCharacter()
		else
			Load()
mob/proc/redoStat(list/_statMods)
	strengthMod = _statMods["STRENGTH"] * 0.1
	forceMod = _statMods["FORCE"] * 0.1
	offenseMod = _statMods["OFFENSE"] * 0.1
	defenseMod = _statMods["DEFENSE"] * 0.1
	durabilityMod = _statMods["DURABILITY"] * 0.1
	resistanceMod = _statMods["RESISTANCE"] * 0.1
	regenMod = _statMods["REGENERATION"] * 0.1
	recovMod = _statMods["RECOVERY"] * 0.1
	energyMod = _statMods["ENERGY"] * 0.1
mob/proc/baseStat(list/_baseStats)
	baseStrength = _baseStats["STRENGTH"]
	baseForce = _baseStats["FORCE"]
	baseOffense = _baseStats["OFFENSE"]
	baseDefense = _baseStats["DEFENSE"]
	baseDurability = _baseStats["DURABILITY"]
	baseResistance = _baseStats["RESISTANCE"]
	baseEnergy = _baseStats["ENERGY"]
	basePowerLevel = _baseStats["POWERLEVEL"]
	powerLevelMod = _baseStats["POWERLEVELMOD"]
mob/proc/newCharacterSetup()
	var/race/R = global.races[race]
	for(var/skill/S in R.skills)
		if(!(S in illegalSkills))
			skills.Add(S)
	baseStat(R.stats)
	icon = R.icons["[gender][R.id].dmi"]
	fullHeal()
	toSpawn()
mob/proc/fullHeal()
mob/proc/toSpawn()