turf
	var flightEnter
	Enter(atom/A)
		if(flightEnter && ismob(A))
			var/mob/M = A
			if(M.flight)
				return TRUE
		if(!flightEnter && !density)
			return TRUE
		. = ..()

turf/water
	flightEnter = 1
turf/ground
turf/wall
	density = 1