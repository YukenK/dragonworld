event
	regenHealth
		tickTime = 2
		Tick()
			if(..() && owner.canRegen("HEALTH") )
				owner.healthChange(owner.healthRegen)
	regenEnergy
		tickTime = 2
		Tick()
			if(..() && owner.canRegen("ENERGY"))
				owner.energyChange(owner.energyRegen)
	healthStop
		doesTick = 0
		startTick()
			..()
			owner.doesHealth--
		endTick()
			owner.doesHealth++
			..()
	energyStop
		doesTick = 0
		startTick()
			..()
			owner.doesEnergy--
		endTick()
			owner.doesEnergy++
			..()
	regenStop
		doesTick = 0
		startTick()
			owner.doesRegen--
		endTick()
			owner.doesRegen++
mob
	proc
		canRegen(type)
			if(!doesRegen)
				return FALSE
			else if(type == "HEALTH" && !doesHealth)
				return FALSE
			else if(type == "ENERGY" && !doesEnergy)
				return FALSE
			return TRUE
		healthChange(i)
			health = clamp(health + i, 0, maxHealth)
		energyChange(i)
			energy = clamp(energy + i, 0, maxEnergy)