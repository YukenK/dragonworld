client
	var list/keys = list()
	verb/keyDown(k as text)
		set hidden = TRUE
		keys.Add(k)
	verb/keyUp(k as text)
		set hidden = TRUE
		keys.Remove(k)
mob
	var vector/velocity
	var vector/skillVelocity
	var accelSpeed
	var maxSpeed
	var decelSpeed
	var moveX
	var moveY
	proc/getInput()
		if(client.keys.Find("North") && client.keys.Find("South"))
			moveY = 0
		else if(client.keys.Find("North"))
			moveY = 1
			if(canTurn())
				dir = NORTH
		else if(client.keys.Find("South"))
			moveY = -1
			if(canTurn())
				dir = SOUTH
		else
			moveY = 0
		if(client.keys.Find("East") && client.keys.Find("West"))
			moveX = 0
		else if(client.keys.Find("East"))
			moveX = 1
			if(canTurn())
				dir = EAST
		else if(client.keys.Find("West"))
			moveX = -1
			if(canTurn())
				dir = WEST
		else
			moveX = 0
	proc/moveLoop()
		getInput()
		Acceleration()
		if(canMove())
			Translate((velocity.x + skillVelocity.x) * world.tick_lag, 0)
			Translate(0, (velocity.y + skillVelocity.y) * world.tick_lag)
		else
			Translate(skillVelocity.x * world.tick_lag, 0)
			Translate(0, skillVelocity.y * world.tick_lag)
	proc/Acceleration()
		if(moveX != 0)
			velocity.x = clamp(velocity.x + (accelSpeed * world.tick_lag) * moveX, -maxSpeed, maxSpeed)
		else if(velocity.x > 0)
			velocity.x = clamp(velocity.x - decelSpeed * world.tick_lag, 0, maxSpeed)
		else if(velocity.x < 0)
			velocity.x = clamp(velocity.x + decelSpeed * world.tick_lag, -maxSpeed, 0)
		if(moveY != 0)
			velocity.y = clamp(velocity.y + (accelSpeed * world.tick_lag) * moveY, -maxSpeed, maxSpeed)
		else if(velocity.y > 0)
			velocity.y = clamp(velocity.y - decelSpeed * world.tick_lag, 0, maxSpeed)
		else if(velocity.y < 0)
			velocity.y = clamp(velocity.y + decelSpeed * world.tick_lag, -maxSpeed, 0)
	proc/canMove()
		return TRUE
	proc/canTurn()
		return TRUE