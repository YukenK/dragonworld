var list/statList = list("STRENGTH", "FORCE", "OFFENSE", "DEFENSE", "DURABILITY", "RESISTANCE", "REGENERATION", "RECOVERY", "ENERGY")
mob
	var list/statSetup = list()
	var statPoints = 60
	var list/statMax = list()
	var list/statMin = list()

	var statChoosing
	var statFinished // Used to check if stats were finished, or canceled.
	verb
		addStat(statName as text)
			set name = ".addStat"
			if(statPoints && statSetup[statName] < statMax[statName])
				statSetup[statName]++
				statPoints--
				statSet()
		removeStat(statName as text)
			set name = ".removeStat"
			if(statSetup[statName] > statMin[statName])
				statSetup[statName]--
				statPoints++
				statSet()
		cancelStat()
			set name = ".cancelStat"
			statChoosing = 0
			winshow(usr, "stats", 0)
		finishStat()
			set name = ".finishStat"
			if(statPoints)
				alert("You still have stat points remaining! Distribute them before continuing.")
				return
			statChoosing = 0
			statFinished = 1
			winshow(usr, "stats", 0)
		statInfo()
			set name = ".statInfo"
			usr << output("This selection screen is to set your stat modifiers - if the stat cap is 200 and you have a 2x Strength modifier, you would have a maximum of 400 base strength for example,", "chat")
	proc/startStats(list/_statMin, list/_statMax, _statPoints = 55, _cancel)
		statChoosing = 1
		statFinished = 0
		statPoints = _statPoints
		statMin = _statMin
		statMax = _statMax
		for(var/STAT in statList)
			statSetup[STAT] = statMin[STAT]
		statSet()
		winshow(src, "stats", 1)
		if(_cancel)
			winshow(src, "stats.cancel", 1)
		else
			winshow(src, "stats.cancel", 0)
		statSet()
	proc/statSet() // Set stat labels.
		for(var/STAT in statList)
			winset(src, "stats.[STAT]", "text = [statSetup[STAT] * 0.1]")
		winset(usr, "stats.points", "text = \"Points: [statPoints]\"")