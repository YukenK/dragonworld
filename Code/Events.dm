var list/eventPlayers = list()
proc/eventLoop()
	set waitfor = 0
	while(eventPlayers.len)
		for(var/mob/M in eventPlayers)
			M.eventLoop()
			M.moveLoop()
		sleep(world.tick_lag)
mob
	var list/events = list()
	proc/eventLoop()
		eventRemove()
		for(var/event/E in events)
			if(E.doesTick)
				E.Tick()
event
	var mob/owner
	var list/targets = list() // Used when the event has multiple targets.
	var targetRemove // If one of the targets no longer has this event, it will be removed from all of them.
	var nextTick
	var tickTime
	var doesTick = 1
	New(mob/_owner, list/_targets, _targetRemove)
		owner = _owner
		targets = _targets
		targetRemove = _targetRemove
		startTick()
	proc/Tick()
		if(nextTick - world.time <= world.tick_lag)
			tickTime ? (nextTick = world.time + tickTime) : (nextTick = world.time + world.tick_lag)
			return TRUE
	proc/startTick()
		owner.events.Add(src)
		if(targets)
			for(var/mob/M in targets)
				M.events.Add(src)
	proc/endTick()
		owner.events.Remove(src)
		if(targets)
			for(var/mob/M in targets)
				M.events.Remove(src)
		del(src)
event/timed
	var time
	New(mob/_owner, list/_targets, _targetRemove, _time)
		..()
		time = _time
		if(time > 0)
			time += world.time
mob
	proc/eventRemove()
		for(var/event/timed/E in events)
			if(world.time > E.time && E.time != -1)
				E.endTick()