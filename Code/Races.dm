var
	raceManager/raceManager = new()
	list/races = list()
	list/allowedRaces = list()
	list/disabledRaces = list()
	list/defaultStats = list("STRENGTH" = 1, "FORCE" = 1, "OFFENSE" = 1, "DEFENSE" = 1, "DURABILITY" = 1, "RESISTANCE" = 1, "ENERGY" = 100, "POWERLEVEL" = 1, "POWERLEVELMOD" = 1, "INTELLIGENCE" = 1)
raceManager
	New()
		var/race/R, raceID
		for(var/race in typesof(/race))
			R = race
			if(raceID = initial(R.id))
				global.races[raceID] = new race()

race
	var id
	var list/statMin = list("STRENGTH" = 10, "FORCE" = 10, "OFFENSE" = 10, "DEFENSE" = 10, "DURABILITY" = 10, "RESISTANCE" = 10, "REGENERATION" = 10, "RECOVERY" = 10, "ENERGY" = 10) // Used at character creation, stat redos, etc.
	var list/statMax = list("STRENGTH" = 30, "FORCE" = 30, "OFFENSE" = 30, "DEFENSE" = 30, "DURABILITY" = 30, "RESISTANCE" = 30, "REGENERATION" = 25, "RECOVERY" = 25, "ENERGY" = 20)
	var list/stats = list()
	var statPoints = 45
	var list/skills = list()
	var list/icons = list()
	var gendered = 1
race/human
	id = "Human"
	stats = list("STRENGTH" = 1, "FORCE" = 1, "OFFENSE" = 1, "DEFENSE" = 1, "DURABILITY" = 1, "RESISTANCE" = 1, "ENERGY" = 100, "POWERLEVEL" = 1, "POWERLEVELMOD" = 1, "INTELLIGENCE" = 1)
race/namekian
	id = "Namekian"
	gendered = 0
	stats = list("STRENGTH" = 1, "FORCE" = 1, "OFFENSE" = 1, "DEFENSE" = 1, "DURABILITY" = 1, "RESISTANCE" = 1, "ENERGY" = 200, "POWERLEVEL" = 10, "POWERLEVELMOD" = 1.15, "INTELLIGENCE" = 0.8)
	statMin = list("STRENGTH" = 10, "FORCE" = 10, "OFFENSE" = 10, "DEFENSE" = 10, "DURABILITY" = 12, "RESISTANCE" = 12, "REGENERATION" = 25, "RECOVERY" = 20, "ENERGY" = 10)
	statPoints = 20
race/saiyan
	id = "Saiyan"
	stats = list("STRENGTH" = 1, "FORCE" = 1, "OFFENSE" = 1, "DEFENSE" = 1, "DURABILITY" = 1, "RESISTANCE" = 1, "ENERGY" = 100, "POWERLEVEL" = 15, "POWERLEVELMOD" = 1.35, "INTELLIGENCE" = 0.65)
	statMin = list("STRENGTH" = 11, "FORCE" = 10, "OFFENSE" = 10, "DEFENSE" = 10, "DURABILITY" = 11, "RESISTANCE" = 10, "REGENERATION" = 10, "RECOVERY" = 10, "ENERGY" = 10)
	statPoints = 38
race/saiyanElite
	id = "Saiyan Elite"
	stats = list("STRENGTH" = 1, "FORCE" = 1, "OFFENSE" = 1, "DEFENSE" = 1, "DURABILITY" = 1, "RESISTANCE" = 1, "ENERGY" = 100, "POWERLEVEL" = 95, "POWERLEVELMOD" = 1.45, "INTELLIGENCE" = 0.65)
	statMin = list("STRENGTH" = 12, "FORCE" = 10, "OFFENSE" = 12, "DEFENSE" = 10, "DURABILITY" = 12, "RESISTANCE" = 10, "REGENERATION" = 10, "RECOVERY" = 10, "ENERGY" = 10) // Used at character creation, stat redos, etc.
	statPoints = 32