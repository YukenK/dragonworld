mob
	var
		// STATS
		healthRegen
		energyRegen
		health
		maxHealth

		baseEnergy
		energy
		maxEnergy
		energyMod

		doesHealth = 1 // Whether or not player regen. If below 1, false.
		doesEnergy = 1
		doesRegen = 1
		// COMBAT STATS
		basePowerLevel
		powerLevel
		maxPowerLevel
		powerLevelMod

		baseStrength
		strength
		strengthMod

		baseForce
		force
		forceMod

		baseOffense
		offense
		offenseMod

		baseDefense
		defense
		defenseMod

		baseDurability
		durability
		durabilityMod

		baseResistance
		resistance
		resistanceMod

		regenMod
		recovMod
		// RACE STUFF
		race
		// TIME
		lastSeen
		// SKILLS
		flight