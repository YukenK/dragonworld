var list/admins = list()
var list/bans = list()
var list/mutes = list()

admin
	verb
		Ban()
			var list/options = list()
			for(var/mob/M in world)
				options.Add(M.key)
			var input = input("Who do you wish to ban?") in options + list("Cancel")
			if(input == "Cancel")
				return
			else
				var time = input("How long, in hours, do you wish to ban [input]?") as num
				var final = input("Are you sure you wish to ban [input]?") in list("Yes", "Cancel")
				if(final == "Cancel")
					return
				else
					bans.Add(input = (time * 36000) + world.realtime)
				world << output("<font color = red>[input] has been banned by [usr].", "chat")
				for(var/mob/M in world)
					if(M.key == input)
						M.Save()
						del(M)