mob/var/list/skills = list()

var
	list/illegalSkills = list() // Skills that aren't allowed, at all.
	list/disabledSkills = list() // Skills that can not be learned, but enabled via race, admins, etc.
	list/allowedSkills = list()
skill
	var id
	proc/canUse(mob/_owner)
	proc/Use(mob/_owner)
		if(canUse(_owner))
			Used(_owner)
	proc/Used(mob/_owner)